package top.jacktgq.controller;

import cn.dev33.satoken.stp.StpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author xka
 * @Date 2022/4/02 13:41
 * @Description 用户登录后前端控制器
 */
@Controller
public class IndexController {




    //打开管理员主页
    @RequestMapping("/")
    public String xx() {
        return "redirect:/admin/index ";
    }


    //打开管理员主页
    @RequestMapping("/admin/index")
    public String showUpdateUserForm(Model model) {

        System.out.println("当前会话是否登录：" + StpUtil.isLogin());
        String tokenValue = StpUtil.getTokenValue();
        String loginIdAsString = StpUtil.getLoginIdAsString();
        model.addAttribute("loginIdAsString",loginIdAsString);
        model.addAttribute("tokenValue",tokenValue);
        return "/admin/index";
    }
}
