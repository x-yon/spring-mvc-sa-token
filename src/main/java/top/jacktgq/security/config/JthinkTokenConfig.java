package top.jacktgq.security.config;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.spring.SpringMVCUtil;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaFoxUtil;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.jacktgq.security.util.StpMemberUtil;

import java.util.Arrays;

/**
 * 拦截器配置
 * @author  by xka
 * @date  2022.04.02  13:42
 */
@Configuration
@EnableWebMvc//开启SpringMVC支持，如无此注解，重写WebMvcConfigurer类的方法无效
@ComponentScan("top.jacktgq.controller")
public class JthinkTokenConfig implements WebMvcConfigurer {
	// 注册sa-token的拦截器
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 启用注解可以和下面路由拦截器(SaRouteInterceptor)同时使用
		registry.addInterceptor(new SaAnnotationInterceptor()).addPathPatterns("/**");

		// 注册路由拦截器，自定义验证规则
		registry.addInterceptor(new SaRouteInterceptor((request, response, handler) -> {
			// 登录验证 -- 排除多个路径
			SaRouter.match(Arrays.asList("/admin/index/**"),
					Arrays.asList("/static/**", "/system/adminlogin", "/system/dologin", "/adminlogout"),
					() -> StpUtil.checkLogin());

		/*	// 角色认证 -- 拦截以 admin 开头的路由，必须具备[admin]角色或者[super-admin]角色才可以通过认证
			SaRouter.match("/admin/**", () -> StpUtil.checkRoleOr("admin", "super-admin"));

			// 权限认证 -- 不同模块, 校验不同权限
			SaRouter.match("/user/**", () -> StpUtil.checkPermission("user"));
			SaRouter.match("/admin/**", () -> StpUtil.checkPermission("admin"));
			SaRouter.match("/goods/**", () -> StpUtil.checkPermission("goods"));
			SaRouter.match("/orders/**", () -> StpUtil.checkPermission("orders"));
			SaRouter.match("/notice/**", () -> StpUtil.checkPermission("notice"));
			SaRouter.match("/comment/**", () -> StpUtil.checkPermission("comment"));

			// 匹配 restful 风格路由
			SaRouter.match("/article/get/{id}", () -> StpUtil.checkPermission("article"));*/

			// 在多账号模式下，可以使用任意StpUtil进行校验
			SaRouter.match(Arrays.asList("/member/**"),
					Arrays.asList("/member/login", "/member/dologin", "/member/register", "/member/logout"),
					() -> StpMemberUtil.checkLogin());

		})).addPathPatterns("/**");
	}


	/** 注册 [Sa-Token全局过滤器] */
	@Bean
	public SaServletFilter getSaServletFilter() {
		return new SaServletFilter()
				.addInclude("/**")
				.addExclude("/sso/*", "/favicon.ico")
				.setAuth(obj -> {
					if(StpUtil.isLogin() == false) {
						String back = SaFoxUtil.joinParam(SaHolder.getRequest().getUrl(), SpringMVCUtil.getRequest().getQueryString());
						SaHolder.getResponse().redirect("/sso/login?back=" + SaFoxUtil.encodeUrl(back));
						SaRouter.back();
					}
				})
				;
	}


}
